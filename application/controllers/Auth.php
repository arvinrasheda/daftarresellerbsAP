<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->database();
		$this->load->library('session');
		// $this->load->library('encrypt');
		$this->load->helper('security');
	}
	public function cek_login() {
		$password = $this->input->post('userPassword');
		$pass =  do_hash($password, 'md5');
		$data = array('userEmail' => $this->input->post('userEmail', TRUE),
			//'password' => $pass
			'userPassword' => $pass
			);
		$this->load->model('Model_user'); // load model_user
		$hasil = $this->Model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['loged_in'] = 'Sudah Login';
				$sess_data['mbr_ID'] = $sess->id;
				$sess_data['mbr_foto'] = $sess->userFoto;
				$sess_data['mbr_nama'] = $sess->userName;
				$sess_data['mbr_inv'] = $sess->userTelegram;
				$sess_data['mbr_ks'] = $sess->userPassword;
				$sess_data['mbr_name'] = $sess->userFullname;
				$sess_data['role'] = $sess->userRole;
				$sess_data['last_log'] = $sess->userLastlog;
				$sess_data['curr_date'] = $sess->userLogin;
				$this->session->set_userdata($sess_data);
			}

		$this->db->set('userLastlog',$sess_data['curr_date']);
		$this->db->set('userLogin',date('Y-m-d'));
		$this->db->where('id',$sess_data['mbr_ID']);
		$this->db->update('user');

			if ($sess_data['role'] == 'Developer') {
			echo "<script>alert('Selamat datang, ".$sess_data['mbr_name']."') ; window.location.href = '../Developer/'</script>";
			}
			if ($sess_data['role'] == 'AdminData') {
			echo "<script>alert('Selamat datang, ".$sess_data['mbr_name']."') ; window.location.href = '../AdminData/'</script>";
			}
			if ($sess_data['role'] == 'CRM') {
			echo "<script>alert('Selamat datang, ".$sess_data['mbr_name']."') ; window.location.href = '../CRM/'</script>";
            }
            if ($sess_data['role'] == 'CS') {
                echo "<script>alert('Selamat datang, ".$sess_data['mbr_name']."') ; window.location.href = '../CS/'</script>";
            }
            if ($sess_data['role'] == 'Profiling') {
            echo "<script>alert('Selamat datang, ".$sess_data['mbr_name']."') ; window.location.href = '../Profiling/'</script>";
            }
            if ($sess_data['role'] == 'Shipping') {
            echo "<script>alert('Selamat datang, ".$sess_data['mbr_name']."') ; window.location.href = '../Shipping/'</script>";
            }
		}
		else {
			echo "<script>alert('Login Gagal. Cek Email dan Password anda.');history.go(-1);</script>";
		}
	}

}

?>