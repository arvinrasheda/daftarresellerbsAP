<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class welcome extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('encrypt');
		$this->load->helper('security');
		$this->load->model('Model_reseller');
		$this->load->library('pagination');
	}
	public function index()
	{
	    $aff = $this->input->get('aff');
	    if(empty($aff)){
		    $data1['aff'] = "0";
	    }else{
		$data1['aff'] = $this->input->get('aff');
	    }
		$data1['get_province'] = $this->Model_reseller->get_province();
		$this->load->view ('index', $data1);
	}
	public function pembelian()
	{
		$data1['get_province'] = $this->Model_reseller->get_province();
		$this->load->view ('pembelian', $data1);
	}
	public function pembayaran()
	{
		$data1['get_province'] = $this->Model_reseller->get_province();
		$this->load->view ('pembayaran', $data1);
	}
	public function loadData(){
	  $loadType=$this->input->post('loadType');
	  $loadId=$this->input->post('loadId');
	  $data=$this->Model_reseller->get_load($loadType,$loadId);
	  $HTML="";  
	  if($data->num_rows() > 0){
	   foreach($data->result() as $list){
	    $HTML.="<option value='".$list->id."'>".$list->name."</option>";
	   }
	  }
	  echo $HTML;
	 }

	public function daftar()
	{ 
		$this->Model_reseller->daftar(); 
	}
	public function beli()
	{ 
		$this->Model_reseller->beli(); 
	}

}