<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm extends CI_Controller {
function __construct(){
		parent::__construct();
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		// konfigurasi helper & library
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('pagination');
		$this->load->library('session');
		$this->load->library('form_validation');
		// $this->load->helper('database');
		if ($this->session->userdata('role')!="CRM" ) {
			// $this->session->unset_userdata('mbr_inv');
			session_destroy();

			echo"Anda Belum Login";
			redirect('LandingAdmin');
		}
		$this->load->database();
		$this->load->model('Model_daftarreseller');
		$this->load->model('Model_user');
		// $this->load->model('Model_modul');
		// $this->load->model('Model_materi');
		// $this->load->model('Model_video');
		// $this->load->model('Model_bonus');
		// $this->load->model('Model_grup');
		// $this->load->library('pagination');
	}
	public function index()
	{
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
		$data1['view'] = $this->load->view('admin/crm/dashboard', NULL, TRUE);
        $this->load->view ('admin/skin/index', $data1);
	}
	public function listpendaftaran()
	{
		// konfigurasi class pagination
		$config['base_url']=base_url()."Crm/listpendaftaran";
		$config['total_rows']= $this->db->query("SELECT * FROM resellers;")->num_rows();
		$config['per_page']=10;
	 	$config['num_links'] = 2;
		$config['uri_segment']=3;
		//Tambahan untuk styling
        // $config['full_tag_open'] = "<ul class='pagination'>";
        // $config['full_tag_close'] ="</ul>";
        // $config['num_tag_open'] = '<li>';
        // $config['num_tag_close'] = '</li>';
        // $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        // $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        // $config['next_tag_open'] = "<li>";
        // $config['next_tagl_close'] = "</li>";
        // $config['prev_tag_open'] = "<li>";
        // $config['prev_tagl_close'] = "</li>";
        // $config['first_tag_open'] = "<li>";
        // $config['first_tagl_close'] = "</li>";
        // $config['last_tag_open'] = "<li>";
        // $config['last_tagl_close'] = "</li>";
		$config['first_link']='< Pertama ';
	 	$config['last_link']='Terakhir > ';
	 	$config['next_link']='> ';
	 	$config['prev_link']='< ';
		$this->pagination->initialize($config);
			// konfigurasi model dan view untuk menampilkan data
		$this->load->model('Model_daftarreseller');
		$data['datakontak']=$this->Model_daftarreseller->getAll($config);
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
	 	$data1['view'] = $this->load->view('admin/crm/listpendaftaran', $data, TRUE);
		$this->load->view ('admin/skin/index', $data1);
	}
	public function profile()
	{
		
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
		$data['profile'] = $this->Model_user->tampil_data();
		$data1['view'] = $this->load->view('admin/crm/profile', $data, TRUE);
        $this->load->view ('admin/skin/index', $data1);
	}
	public function users()
	{
		
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
		$data1['view'] = $this->load->view('admin/crm/user', NULL, TRUE);
        $this->load->view ('admin/skin/index', $data1);
	}
	public function active()
	{
		
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
		$data1['view'] = $this->load->view('admin/crm/activationlist', NULL, TRUE);
        $this->load->view ('admin/skin/index', $data1);
	}
	public function confirmation()
	{
		
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
		$data1['view'] = $this->load->view('admin/crm/confirmationlist', NULL, TRUE);
        $this->load->view ('admin/skin/index', $data1);
	}
	public function followup()
	{
		
		$data1['nav'] = $this->load->view('admin/crm/navbar', NULL, TRUE);
		$data1['account'] = $this->load->view('admin/crm/account', NULL, TRUE);
		$data1['view'] = $this->load->view('admin/crm/followup', NULL, TRUE);
        $this->load->view ('admin/skin/index', $data1);
	}
	public function edit_user(){
		$config['upload_path']="./assets/admin/upload/profil/foto/";
        $config['allowed_types']='gif|jpg|png';
         
        $this->load->library('upload',$config);
		if($this->upload->do_upload("foto")){
            $data = array('upload_data' => $this->upload->data());
 
            // $judul= $this->input->post('judul');
            $image= $data['upload_data']['file_name']; 
             
			// $result= $this->m_upload->simpan_upload($judul,$image);
			$result= $this->Model_user->simpan_upload($image);
            echo json_decode($result);
        }
        $this->Model_user->edit_user($this->upload->data());// pass the uploaded information to the model
			echo "<script>
				  alert('Data berhasil diubah!');
				  window.location.href='".base_url()."Crm/profile/';
			      </script>";
	}

	// public function tambah_member()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_member->tambah_member(); 
	// 	echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/member';
	// 	 </script>";	
	// }

	// public function hapus_member()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_member->hapus_member(); 
	// 	echo "<script>alert('Data berhasil dihapus!');
	// 			window.location.href='".base_url()."index.php/Admin/member';
	// 	 </script>";	
	// }



	// function tambah_member_masal(){
    //     $fileName = time().$_FILES['file']['name'];
         
    //     $config['upload_path'] = FCPATH.'assets/'; //buat folder dengan nama assets di root folder
    //     $config['file_name'] = $fileName;
    //     $config['allowed_types'] = 'xls|xlsx|csv';
    //     $config['max_size'] = 10000;
         
    //     $this->load->library('upload');
    //     $this->upload->initialize($config);
         
    //     if(! $this->upload->do_upload('file') )
    //     $this->upload->display_errors();
             
    //     $media = $this->upload->data('file');
    //     $inputFileName = $this->upload->data('full_path');
         
    //     try {
    //             $inputFileType = IOFactory::identify($inputFileName);
    //             $objReader = IOFactory::createReader($inputFileType);
    //             $objPHPExcel = $objReader->load($inputFileName);
    //         } catch(Exception $e) {
    //             die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    //         }
            
 
    //         $sheet = $objPHPExcel->getSheet(0);
    //         $highestRow = $sheet->getHighestRow();
    //         $highestColumn = $sheet->getHighestColumn();
             
    //         for ($row = 2; $row < $highestRow; $row++){                  //  Read a row of data into an array                 
    //             $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
    //                                             NULL,
    //                                             TRUE,
    //                                             FALSE);
                                                 
	// 			//generate id baru untuk setiap row-nya
	// 			$mbr_ID = $this->Model_member->getid_member();

	// 			$data = array(
    //                 "mbr_telp"=> $rowData[0][2]
    //             );

	// 			$mbr_ks = substr($data['mbr_telp'],-5);
	// 			$pass =  do_hash($mbr_ks, 'md5');

    //             //Sesuaikan sama nama kolom tabel di database       
	// 			$datas = array(
    //                 //"idimport"=> $rowData[0][0],
    //                 "mbr_ID"=> $mbr_ID,
    //                 "mbr_nama"=> $rowData[0][0],
    //                 "mbr_email"=> $rowData[0][1],
    //                 "mbr_hp"=> $rowData[0][2],
    //                 "mbr_inv"=> $rowData[0][3],
    //                 "mbr_un"=> $rowData[0][1],
    //                 "mbr_ks"=> $pass,
    //                 "role"=> "user"
    //             );
    //             //sesuaikan nama dengan nama tabel
    //             $insert = $this->db->insert("member",$datas);
    //             //delete_files($media['file_path']);
                   
    //         }

	// 		echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/member';
	// 		  </script>";	
    // }
	// //------------------------ MATERI -----------------------//
	// public function materi()
	// {
	// 	//$data['namauser'] = $this->session->userdata('namauser');
	// 	//$data['namapengguna'] = $this->session->userdata('namapengguna');
	// 	$data['get_materi'] = $this->Model_materi->get_materi();
	// 	$data['getid_materi'] = $this->Model_materi->getid_materi();
	// 	//$data$this->load->view('admin/index',$data);
	// 	$data1['view'] = $this->load->view('admin/materi', $data, TRUE);
	// 	$data1['sidebar'] = $this->load->view('admin/sidebar', NULL, TRUE);
	// 	$this->load->view ('skin/index', $data1);
	// }
	// public function tambah_materi()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_materi->tambah_materi(); 
	// 	echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/materi';
	// 		  </script>";	
	// }
	// function tambah_materi_masal(){
    //     $fileName = time().$_FILES['file']['name'];
         
    //     $config['upload_path'] = FCPATH.'assets/'; //buat folder dengan nama assets di root folder
    //     $config['file_name'] = $fileName;
    //     $config['allowed_types'] = 'xls|xlsx|csv';
    //     $config['max_size'] = 10000;
         
    //     $this->load->library('upload');
    //     $this->upload->initialize($config);
         
    //     if(! $this->upload->do_upload('file') )
    //     $this->upload->display_errors();
             
    //     $media = $this->upload->data('file');
    //     $inputFileName = $this->upload->data('full_path');
         
    //     try {
    //             $inputFileType = IOFactory::identify($inputFileName);
    //             $objReader = IOFactory::createReader($inputFileType);
    //             $objPHPExcel = $objReader->load($inputFileName);
    //         } catch(Exception $e) {
    //             die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    //         }
            
 
    //         $sheet = $objPHPExcel->getSheet(0);
    //         $highestRow = $sheet->getHighestRow();
    //         $highestColumn = $sheet->getHighestColumn();
             
    //         for ($row = 3; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
    //             $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
    //                                             NULL,
    //                                             TRUE,
    //                                             FALSE);
                                                 
	// 			//generate id baru untuk setiap row-nya
	// 			$kat_ID = $this->Model_materi->getid_materi();				                                                 
    //             //Sesuaikan sama nama kolom tabel di database       
	// 			$datas = array(
    //                 //"idimport"=> $rowData[0][0],
    //                 "kat_ID"=> $kat_ID,
    //                 "kat_nama"=> $rowData[0][1],
    //                 "kat_judul"=> $rowData[0][2],
    //                 "kat_rilis"=> $rowData[0][3]
    //             );
    //             //sesuaikan nama dengan nama tabel
    //             $insert = $this->db->insert("kategori",$datas);
    //             //delete_files($media['file_path']);
                   
    //         }

	// 		echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/materi';
	// 		  </script>";	
    // }
	// function hapus_materi()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_materi->hapus_materi();
	// 	echo "<script>alert('Data berhasil dihapus!');
	// 			window.location.href='".base_url()."index.php/Admin/materi';
	// 		  </script>";	
	// }
	// public function edit_materi()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_materi->edit_materi(); 
	// 	echo "<script>alert('Data berhasil diubah!');
	// 			window.location.href='".base_url()."index.php/Admin/materi';
	// 		  </script>";	
	// }
	// //-------------------- END OF MATERI --------------------//


	// //------------------------ MODUL -----------------------//

   	// 	function tambah_modul(){
   	// 		$number_of_files = sizeof($_FILES['file']['tmp_name']);  
    //                      $files = $_FILES['file'];  
    //                              $config=array(  
    //                              'upload_path' => './assets/uploads/modul/', //direktori untuk menyimpan gambar  
    //                              'allowed_types' => 'pdf',  
    //                              'max_size' => '2000',  
    //                              'max_width' => '2000',  
    //                              'max_height' => '2000'  
    //                              );  
    //                      for ($i = 0;$i < $number_of_files; $i++)  
    //                      {  
    //                              $_FILES['file']['name'] = $files['name'][$i];  
    //                              $_FILES['file']['type'] = $files['type'][$i];  
    //                              $_FILES['file']['tmp_name'] = $files['tmp_name'][$i];  
    //                              $_FILES['file']['error'] = $files['error'][$i];  
    //                              $_FILES['file']['size'] = $files['size'][$i];

    //                              $this->load->library('upload', $config);  
    //                              $this->upload->do_upload('file');  
    //     						 $this->Model_modul->tambah_modul($this->upload->data());// pass the uploaded information to the model}
    //                      }  

	// 	echo "<script>alert('Modul berhasil ditambahkan!');
	// 		window.location.href='".base_url()."index.php/Admin/modul';
	// 	 </script>";	
   	// 	}

	// public function mod_materi(){
	//   $loadType = $this->input->post('loadType');
	//   $loadId = $this->input->post('loadId');

	// 	$this->db->set('kat_ID',$loadId);
	// 	$this->db->where('mod_ID',$loadType);
	// 	$this->db->update('modul');
	// }
	// public function mod_thumb(){
	// 	$config['upload_path'] = './assets/uploads/modul/thumbnails';
    //     $config['allowed_types'] = 'gif|jpg|png';

    //     $this->load->library('upload', $config);
    //     $this->upload->do_upload('foto');//upload the file to the above mentioned path
    //     $this->Model_modul->mod_thumb($this->upload->data());// pass the uploaded information to the model}

	// 	echo "<script>alert('Cover berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/modul';
	// 		  </script>";	
	// }
	// public function modul()
	// {
	// 	//$data['namauser'] = $this->session->userdata('namauser');
	// 	//$data['namapengguna'] = $this->session->userdata('namapengguna');
	// 	$data['get_modul'] = $this->Model_modul->get_modul();
	// 	$data['getid_modul'] = $this->Model_modul->getid_modul();
	// 	$data['get_materi'] = $this->Model_materi->get_materi();
	// 	//$data$this->load->view('admin/index',$data);
	// 	$data1['view'] = $this->load->view('admin/modul', $data, TRUE);
	// 	$data1['sidebar'] = $this->load->view('admin/sidebar', NULL, TRUE);
	// 	$this->load->view ('skin/index', $data1);
	// }
	// function hapus_modul()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_modul->hapus_modul();
	// 	echo "<script>alert('Data berhasil dihapus!');
	// 		window.location.href='".base_url()."index.php/Admin/modul';
	// 		  </script>";	
	// }
	// public function tambah_modul_dropbox()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_modul->tambah_modul_dropbox(); 
	// 	echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/modul';
	// 		  </script>";	
	// }

	// public function edit_modul(){
	// 	$foto = $_FILES['foto_e']['name'];

	// 	if ($foto != "") {
	// 	$ID = $this->input->post('id_e');

	// 	$query = $this->db->query('select mod_cover from modul where mod_ID = "'.$ID.'" ');
	// 	$mod_cover = $query->result_array();
	// 				$data = array();
	// 					foreach ($mod_cover as $key => $value) {
	// 						$data[0] = $value['mod_cover'];
	// 					}

	//      		unlink("./assets/uploads/modul/thumbnails/$data[0]"); //delete previous file

	// 	$config['upload_path'] = './assets/uploads/modul/thumbnails';
    //     $config['allowed_types'] = 'gif|jpg|png';

    //     $this->load->library('upload', $config);
    //     $this->upload->do_upload('foto_e');//upload the file to the above mentioned path
    //     $this->Model_modul->edit_modul($this->upload->data());// pass the uploaded information to the model}
	// 	}else{
    //     $this->Model_modul->edit_modul($foto);// pass the uploaded information to the model}
	// 	}
	// 	echo "<script>alert('Data berhasil diubah!');
	// 			window.location.href='".base_url()."index.php/Admin/modul';
	// 		  </script>";	
	// }
	// //-------------------- END OF MODUL --------------------//

	// //------------------------ VIDEO -----------------------//

	// public function video()
	// {
	// 	//$data['namauser'] = $this->session->userdata('namauser');
	// 	//$data['namapengguna'] = $this->session->userdata('namapengguna');
	// 	$data['get_video'] = $this->Model_video->get_video();
	// 	$data['getid_video'] = $this->Model_video->getid_video();
	// 	$data['get_materi'] = $this->Model_materi->get_materi();
	// 	//$data$this->load->view('admin/index',$data);
	// 	$data1['view'] = $this->load->view('admin/video', $data, TRUE);
	// 	$data1['sidebar'] = $this->load->view('admin/sidebar', NULL, TRUE);
	// 	$this->load->view ('skin/index', $data1);
	// }

	// function tambah_video(){
   	// 		$number_of_files = sizeof($_FILES['file']['tmp_name']);  
    //                      $files = $_FILES['file'];  
    //                              $config=array(  
    //                              'upload_path' => './assets/uploads/video/', //direktori untuk menyimpan gambar  
    //                              'allowed_types' => 'mp4|flv|3gp'
    //                              );  
    //                      for ($i = 0;$i < $number_of_files; $i++)  
    //                      {  
    //                              $_FILES['file']['name'] = $files['name'][$i];  
    //                              $_FILES['file']['type'] = $files['type'][$i];  
    //                              $_FILES['file']['tmp_name'] = $files['tmp_name'][$i];  
    //                              $_FILES['file']['error'] = $files['error'][$i];  
    //                              $_FILES['file']['size'] = $files['size'][$i];

    //                              $this->load->library('upload', $config);  
    //                              $this->upload->do_upload('file');  
    //     						 $this->Model_video->tambah_video($this->upload->data());// pass the uploaded information to the model}
    //                      }  

	// 	echo "<script>alert('Video berhasil ditambahkan!');
	// 		window.location.href='".base_url()."index.php/Admin/video';
	// 	 </script>";	
   	// }

	// public function tambah_video_youtube()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_video->tambah_video_youtube(); 
	// 	echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/video';
	// 		  </script>";	
	// }

	// public function vid_materi(){
	//   $loadType = $this->input->post('loadType');
	//   $loadId = $this->input->post('loadId');

	// 	$this->db->set('kat_ID',$loadId);
	// 	$this->db->where('vid_ID',$loadType);
	// 	$this->db->update('video');
	// }

	// function hapus_video()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_video->hapus_video();
	// 	echo "<script>alert('Data berhasil dihapus!');
	// 		window.location.href='".base_url()."index.php/Admin/video';
	// 		  </script>";	
	// }

	// public function edit_video(){
		
    //     $this->Model_video->edit_video();// pass the uploaded information to the model}
	// 	echo "<script>alert('Data berhasil diubah!');
	// 			window.location.href='".base_url()."index.php/Admin/video';
	// 		  </script>";	
	// }
	// //-------------------- END OF VIDEO --------------------//


	// //------------------------ BONUS -----------------------//

	// public function bonus()
	// {
	// 	//$data['namauser'] = $this->session->userdata('namauser');
	// 	//$data['namapengguna'] = $this->session->userdata('namapengguna');
	// 	$data['get_bonus'] = $this->Model_bonus->get_bonus();
	// 	$data['getid_bonus'] = $this->Model_bonus->getid_bonus();
	// 	$data['get_materi'] = $this->Model_materi->get_materi();
	// 	//$data$this->load->view('admin/index',$data);
	// 	$data1['view'] = $this->load->view('admin/bonus', $data, TRUE);
	// 	$data1['sidebar'] = $this->load->view('admin/sidebar', NULL, TRUE);
	// 	$this->load->view ('skin/index', $data1);
	// }
	// function tambah_bonus(){
   	// 		$number_of_files = sizeof($_FILES['file']['tmp_name']);  
    //                      $files = $_FILES['file'];  
    //                              $config=array(  
    //                              'upload_path' => './assets/uploads/bonus/', //direktori untuk menyimpan gambar  
    //                              'allowed_types' => 'mp4|flv|3gp|pdf|gif|jpg|png|xls|doc|ppt|xlsx'
    //                              );  
    //                      for ($i = 0;$i < $number_of_files; $i++)  
    //                      {  
    //                              $_FILES['file']['name'] = $files['name'][$i];  
    //                              $_FILES['file']['type'] = $files['type'][$i];  
    //                              $_FILES['file']['tmp_name'] = $files['tmp_name'][$i];  
    //                              $_FILES['file']['error'] = $files['error'][$i];  
    //                              $_FILES['file']['size'] = $files['size'][$i];

    //                              $this->load->library('upload', $config);  
    //                              $this->upload->do_upload('file');  
    //     						 $this->Model_bonus->tambah_bonus($this->upload->data());// pass the uploaded information to the model}
    //                      }   
	// 	echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/bonus';
	// 		  </script>";	
    // }

	// public function tambah_bonus_tautan()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_bonus->tambah_bonus_tautan(); 
	// 	echo "<script>alert('Data berhasil ditambahkan!');
	// 			window.location.href='".base_url()."index.php/Admin/bonus';
	// 		  </script>";	
	// }


	// function hapus_bonus()
	// {
	// 	//$data['email'] = $this->session->userdata('email');
	// 	//$this->Data_pemilik->update_data(); 
	// 	$this->Model_bonus->hapus_bonus();
	// 	echo "<script>alert('Data berhasil dihapus!');
	// 		window.location.href='".base_url()."index.php/Admin/bonus';
	// 		  </script>";	
	// }

	// public function bon_materi(){
	//   $loadType = $this->input->post('loadType');
	//   $loadId = $this->input->post('loadId');

	// 	$this->db->set('kat_ID',$loadId);
	// 	$this->db->where('bon_ID',$loadType);
	// 	$this->db->update('bonus');
	// }

	// public function edit_bonus(){
		
    //     $this->Model_bonus->edit_bonus();// pass the uploaded information to the model}
	// 	echo "<script>alert('Data berhasil diubah!');
	// 			window.location.href='".base_url()."index.php/Admin/bonus';
	// 		  </script>";	
	// }

	// //-------------------- END OF VIDEO --------------------//


	// //------------------------ BONUS -----------------------//

	// public function grup()
	// {
	// 	//$data['namauser'] = $this->session->userdata('namauser');
	// 	//$data['namapengguna'] = $this->session->userdata('namapengguna');
	// 	$data['grup_member'] = $this->Model_grup->grup_member();
	// 	$data['grup_alumni'] = $this->Model_grup->grup_alumni();
	// 	//$data$this->load->view('admin/index',$data);
	// 	$data1['view'] = $this->load->view('admin/grup', $data, TRUE);
	// 	$data1['sidebar'] = $this->load->view('admin/sidebar', NULL, TRUE);
	// 	$this->load->view ('skin/index', $data1);
	// }
	// public function edit_grupMember(){
		
    //     $this->Model_grup->edit_grupMember();// pass the uploaded information to the model}
	// 	echo "<script>alert('Data berhasil diubah!');
	// 			window.location.href='".base_url()."index.php/Admin/grup';
	// 		  </script>";	
	// }
	// public function edit_grupAlumni(){
		
    //     $this->Model_grup->edit_grupAlumni();// pass the uploaded information to the model}
	// 	echo "<script>alert('Data berhasil diubah!');
	// 			window.location.href='".base_url()."index.php/Admin/grup';
	// 		  </script>";	
	// }

	public function logout() {
		$this->session->unset_userdata('userEmail');
		$this->session->unset_userdata('role');
		session_destroy();
		redirect('LandingAdmin');
	}

	//-------------------- END OF VIDEO --------------------//


	//------------------------ PROFIL -----------------------//



	//-------------------- END OF PROFIL --------------------//
	
}
