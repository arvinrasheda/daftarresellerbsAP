<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="<?php echo base_url() ?>assets/admin/img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>assets/admin/img/favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>Dashboard <?php if($this->session->userdata('role') == 'Developer'){
							echo "Developer";
							}else if($this->session->userdata('role') == 'AdminData'){
							echo "AdminData";
							}else if($this->session->userdata('role') == 'CRM'){
							echo "CRM";
							}else if($this->session->userdata('role') == 'CS'){
              echo "CS";
              }else if($this->session->userdata('role') == 'Shipping'){
              echo "Shipping";
              }else if($this->session->userdata('role') == 'Profiling'){
              echo "Profiling";
              }?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="<?php echo base_url() ?>assets/admin/js/require.min.js"></script>
    <!-- Dashboard Core -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/css/dashboard.css"/>
    <script src="<?php echo base_url() ?>assets/admin/js/dashboard.js"></script>
    <!-- c3.js Charts Plugin -->
    <link rel="stylesheet"  href="<?php echo base_url() ?>assets/admin/plugins/charts-c3/plugin.css"/>
    <script src="<?php echo base_url() ?>assets/admin/plugins/charts-c3/plugin.js"></script>
    <!-- Google Maps Plugin -->
    <link rel="stylesheet"  href="<?php echo base_url() ?>assets/admin/plugins/maps-google/plugin.css"/>
    <script src="<?php echo base_url() ?>assets/admin/plugins/maps-google/plugin.js"></script>
    <!-- Input Mask Plugin -->
    <script src="<?php echo base_url() ?>assets/admin/plugins/input-mask/plugin.js"></script>
  </head>
  <body class="">
    <div class="page">
      <div class="page-main">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="<?php echo base_url().'Developer/' ?>">
                <img src="<?php echo base_url() ?>assets/admin/img/image/logo.png" class="header-brand-img" alt="tabler logo">
              </a>
              <div class="d-flex order-lg-2 ml-auto">                
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url(<?php echo base_url() ?>assets/admin/upload/profil/foto/<?php echo $this->session->userdata('mbr_foto'); ?>)"></span> 
                    <!-- <span class="avatar" style="background-image: url(<?php echo base_url() ?>assets/admin/uploads/profil/foto/<?php echo $mod_thumb->userFoto ?>)"></span> -->
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default"><?php echo $this->session->userdata("mbr_name"); ?></span>
                      <small class="text-muted d-block mt-1">
                            <?php if($this->session->userdata('role') == 'Developer'){
							echo "Developer";
							}else if($this->session->userdata('role') == 'AdminData'){
							echo "AdminData";
							}else if($this->session->userdata('role') == 'CRM'){
							echo "CRM";
							}else if($this->session->userdata('role') == 'CS'){
                            echo "CS";
                            }else if($this->session->userdata('role') == 'Shipping'){
                            echo "Shipping";
                            }else if($this->session->userdata('role') == 'Profiling'){
                            echo "Profiling";
                            }?> | Last Access : <?php echo $this->session->userdata('last_log'); ?></small>
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <!-- Disini Acc -->
                  <?php echo $account; ?>
                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0 sticky-top" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-3 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                  <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search"></i>
                  </div>
                </form>
              </div>
              <!-- Disini Navigasi -->
              <?php echo $nav; ?>
            </div>
          </div>
        </div>
            <!-- Disini Body nya bro -->
            <?php echo $view; ?>
            <!-- Disini Udah Footer -->
        <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-auto ml-lg-auto">
              <div class="row align-items-center">
                <div class="col-auto">
                  <!-- <ul class="list-inline list-inline-dots mb-0">
                    <li class="list-inline-item"><a href="./docs/index.html">Documentation</a></li>
                    <li class="list-inline-item"><a href="./faq.html">FAQ</a></li>
                  </ul> -->
                </div>
                <!-- <div class="col-auto">
                  <a href="<?php echo base_url('Developer/logout'); ?>" class="btn btn-outline-primary btn-sm">LogOut</a>
                </div> -->
              </div>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
              Copyright © 2018 <a href="http://billionairestore.co.id/">Billionaire Store</a>. All rights reserved.
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
</html>