<div class="my-3 my-md-5">
  <div class="container"> 
    <div class="page-header">
  <h1 class="page-title">
  Dashboard
  </h1>
</div>
<div class="row row-cards">
  <div class="col-sm-6 col-lg-3">
    <div class="card">
      <div class="card-body p-3 text-center">
        <div class="text-right text-green">
          3%
          <i class="fe fe-chevron-up"></i>
        </div>
        <div class="h1 m-0">43</div>
        <div class="text-muted mb-4">New Reseller Today</div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-lg-3">
    <div class="card">
      <div class="card-body p-3 text-center">
        <div class="text-right text-green">
          3%
          <i class="fe fe-chevron-up"></i>
        </div>
        <div class="h1 m-0">17</div>
        <div class="text-muted mb-4">Need Confirmation</div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-lg-3">
    <div class="card">
      <div class="card-body p-3 text-center">
        <div class="text-right text-green">
          3%
          <i class="fe fe-chevron-up"></i>
        </div>
        <div class="h1 m-0">7</div>
        <div class="text-muted mb-4">New Follow Up</div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-lg-3">
    <div class="card">
      <div class="card-body p-3 text-center">
        <div class="text-right text-green">
          3%
          <i class="fe fe-chevron-up"></i>
        </div>
        <div class="h1 m-0">27.3K</div>
        <div class="text-muted mb-4">Shipping</div>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck">
  <div class="col-6">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Grafik Daily/Month</h3>
      </div>
      <div id="chart-employment" style="height: 16rem"></div>
    </div>
    <script>
      require(['c3', 'jquery'], function (c3, $) {
        $(document).ready(function () {
          var chart = c3.generate({
            bindto: '#chart-employment', // id of chart wrapper
            data: {
              columns: [
                // each columns data
                ['data1', 2, 8, 6, 7, 14, 11],
                ['data2', 5, 15, 11, 15, 21, 25],
                ['data3', 17, 18, 21, 20, 30, 29]
              ],
              type: 'line', // default type of chart
              colors: {
                'data1': tabler.colors["orange"],
                'data2': tabler.colors["blue"],
                'data3': tabler.colors["green"]
              },
              names: {
                // name of each serie
                'data1': 'Development',
                'data2': 'Marketing',
                'data3': 'Sales'
              }
            },
            axis: {
              x: {
                type: 'category',
                // name of each category
                categories: ['2013', '2014', '2015', '2016', '2017', '2018']
              },
            },
            legend: {
              show: false, //hide legend
            },
            padding: {
              bottom: 0,
              top: 0
            },
          });
        });
      });
    </script>
  </div>
  <div class="col-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Leaderboard</h4>
      </div>
      <table class="table card-table">
        <tbody>
          <tr>
            <td width="1">
              <i class="fa fa-trophy text-muted"></i>
            </td>
            <td>Google Chrome</td>
            <td class="text-right">
              <div class="item-action dropdown">
                <a href="javascript:void(0)" data-toggle="dropdown" class="icon">
                  <i class="fe fe-more-vertical"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="javascript:void(0)" class="dropdown-item">
                    <i class="dropdown-icon fe fe-tag"></i> Action </a>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <i class="fa fa-trophy text-muted"></i>
            </td>
            <td>Mozila Firefox</td>
            <td class="text-right">
              <span class="text-muted">15%</span>
            </td>
          </tr>
          <tr>
            <td>
              <i class="fa fa-trophy text-muted"></i>
            </td>
            <td>Apple Safari</td>
            <td class="text-right">
              <span class="text-muted">7%</span>
            </td>
          </tr>
          <tr>
            <td>
              <i class="fa fa-trophy text-muted"></i>
            </td>
            <td>Internet Explorer</td>
            <td class="text-right">
              <span class="text-muted">9%</span>
            </td>
          </tr>
          <tr>
            <td>
              <i class="fa fa-trophy text-muted"></i>
            </td>
            <td>Opera mini</td>
            <td class="text-right">
              <span class="text-muted">23%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
    </div>
    </div>