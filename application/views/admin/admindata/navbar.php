<div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="<?php echo base_url().'Admindata/' ?>" class="nav-link active"><i class="fe fe-home"></i> Home</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url().'Admindata/listpendaftaran' ?>" class="nav-link"><i class="fe fe-list"></i> List Pendaftaran</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url().'Admindata/followup' ?>" class="nav-link"><i class="fe fe-corner-right-up"></i> Follow Up</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url().'Admindata/confirmation' ?>" class="nav-link"><i class="fe fe-clipboard"></i> Confirmation List</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="<?php echo base_url().'Admindata/active' ?>" class="nav-link"><i class="fe fe-check-square"></i> Activation List</a>
                  </li>
                </ul>
              </div>