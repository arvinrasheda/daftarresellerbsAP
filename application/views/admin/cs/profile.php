<div class="header py-4">
<div class="container">
<div class="page-header">
  <h1 class="page-title">
    Profil Saya
  </h1>
</div>
<?php foreach ($profile as $r): ?>
<div class="row">
  <div class="col-lg-4">
    <div class="card">
      <div class="card-body">
        <div class="media"> 
          <!-- <span class="avatar avatar-xxl mr-5" style="background-image: url(<?php echo base_url() ?>assets/admin/img/avatar.png)"></span> -->
          <span class="avatar avatar-xxl mr-5" style="background-image: url(<?php echo base_url() ?>assets/admin/upload/profil/foto/<?php echo $r->userFoto ?>)"></span>
          <div class="media-body">
            <h4 class="m-0"><?php echo $r->userFullname ?></h4>
            <p class="text-muted mb-0"><?php echo $r->userRole ?>r</p>
            <ul class="social-links list-inline mb-0 mt-2">
              <li class="list-inline-item">
                <a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="Facebook">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="Twitter">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="1234567890">
                  <i class="fa fa-phone"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="@skypename">
                  <i class="fa fa-skype"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-8">
    <form id="submit" enctype="multipart/form-data" accept-charset="utf-8" class="card" action="<?php echo base_url();?>Cs/edit_user" method="POST">
    <!-- <form enctype="multipart/form-data" accept-charset="utf-8" class="card" id="submit">   -->
    <div class="card-body">
        <h3 class="card-title">
          <i class="fe fe-edit mr-1"></i> Edit Profile</h3>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label">Username</label>
              <input name= "username" id="username" class="form-control" placeholder="Username" value="<?php echo $r->userName ?>" type="text" readonly>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label">Email address</label>
              <input name="email" class="form-control" placeholder="Email" type="email" value="<?php echo $r->userEmail ?>">
            </div>
          </div>
          <div class="col-sm-6 col-md-6">
            <div class="form-group">
              <label class="form-label">Nama lengkap</label>
              <input name="fullname" class="form-control" placeholder="Company" value="<?php echo $r->userFullname ?>" type="text">
            </div>
          </div>
          <div class="col-sm-6 col-md-6">
            <div class="form-group">
              <label class="form-label">Telegram</label>
              <input name="telegram" class="form-control" placeholder="Last Name" value="<?php echo $r->userTelegram ?>" type="text">
            </div>
          </div>
          <div class="col-sm-6 col-md-6">
            <div class="form-group">
              <label class="form-label">Password</label>
              <input name="password" class="form-control" placeholder="Last Name" value="<?php echo $r->userPassword ?>" type="password">
            </div>
          </div>
          <div class="col-sm-6 col-md-6">
            <div class="form-group">
            <div class="form-label">Upload Foto Profil</div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="foto">
                    <label class="custom-file-label">Pilih file</label>
                </div>
            </div>
            </div>
            </div>
              <input type="hidden" name="id_user" value="<?php echo $r->id ?>">
          <!-- <div class="col-md-12">
            <div class="form-group mb-0">
              <label class="form-label">Address</label>
              <textarea rows="3" class="form-control" placeholder="Your Address">ABC</textarea>
            </div>
          </div> -->
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary" name="status">
        <i class="fe fe-edit mr-1"></i>Update Profile</button>
      </div>
    </form>
  </div>
  <?php endforeach ?>
</div>
</div>
</div>