<div class="my-3 my-md-5">
<div class="container">
<div class="page-header">
  <h1 class="page-title">
    List Pendaftaran
  </h1>
</div>
<div class="row">
  <div class="col">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Reseller</h3>
      </div>
      <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
          <thead>
            <tr>
              <td>ResID</td>
              <td>Nama Reseller</td>
              <td>Email</td>
              <td>Alamat</td>
              <td>Profesi</td>
              <td>Status</td>
              <td>Price</td>
              <td></td>
              <td></td>
            </tr>
          </thead>
           <tbody>  
           <?php foreach ($datakontak as $r): ?>
            <tr>
              <td>
              <?php echo $r->resID ?>
              </td>
              <td>
              <?php echo $r->resNama ?>
              </td>
              <td>
              <?php echo $r->resEmail ?>
              </td>
              <td>
              <?php echo $r->resAlamat ?>
              </td>
              <td>
              <?php echo $r->resProfesi ?>
              </td>
              <td>
                <span class="status-icon bg-success"></span> Paid
              </td>
              <td>$887</td>
              <td class="text-right">
                <a href="javascript:void(0)" class="btn btn-secondary btn-sm">Manage</a>
                <div class="dropdown">
                  <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Actions</button>
                </div>
              </td>
              <td>
                <a class="icon" href="javascript:void(0)">
                  <i class="fe fe-edit"></i>
                </a>
              </td>
            </tr>
            <?php endforeach ?>
          </tbody>
        </table>
            <?php
               echo $this->pagination->create_links();
             ?>
      </div>
    </div>
  </div>
</div>
</div>
</div>