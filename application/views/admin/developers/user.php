<div class="header py-4">
<div class="container">
<div class="page-header">
  <h1 class="page-title">
    List Users
  </h1>
</div>

<div class="row">
  <div class="col">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Users</h3>
        <div class="card-options">
          <a href="javascript:void(0)" class="btn btn-success btn-sm">
            <i class="fe fe-user-plus mr-1"></i>Add User</a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
          <thead>
            <tr>
              <th class="w-1">No.</th>
              <th>Nama</th>
              <th>Role</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <?php foreach ($profile as $r): ?>
            <tr>
              <td>
                <span class="text-muted">1</span>
              </td>
              <td><?php echo $r->userFullname ?></td>
              <td><?php echo $r->userRole ?></td>
              <td class="text-right">
                <a href="javascript:void(0)" class="btn btn-primary btn-sm">
                  <i class="fe fe-edit mr-1"></i>Edit</a>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm">
                  <i class="fe fe-trash mr-1"></i>Hapus</a>
              </td>
            </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  
</div>
</div>
</div>