<!DOCTYPE HTML>
<html class="dua">
<head>
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5NK4MRN');</script>
  <meta charset="UTF-8">
  <title>Pendaftaran Reseller Billionaire</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
            <link src="<?php echo base_url();?>assets/index/form/css/bootstrap-table.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/index/css/styles.css">
    <style type="text/css">       
        html {
            width: 100%;
            height: 100%;
            background: #00b976; /* fallback for old browsers */
            background: -webkit-linear-gradient(to left, #6441A5, #2a0845); /* Chrome 10-25, Safari 5.1-6 */
        }
        #progressbar li.active:before, #progressbar li.active:after {
            background: #fff13c;
            color: black;
        }
        label > input{ /* HIDE RADIO */
          visibility: hidden; /* Makes input not-clickable */
          position: absolute; /* Remove input from document flow */
        }
        label > input + img{ /* IMAGE STYLES */
          cursor:pointer;
          border:2px solid transparent;
        }
        label > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
          border:2px solid #f00;
        }
        label > img{
            min-height: 200px;
            width: auto;
        }

    </style>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NK4MRN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <!-- MultiStep Form -->
<div class="container">
    <div class="row">
        <center>
            <h1>SELAMAT!</h1>
            <h2>TINGGAL SATU LANGKAH LAGI!</h2>
            <h3 >SIMPAN atau CETAK bukti transaksi berikut, lalu lakukan pembayaran sesuai nominal tertera</h3>
            <input type="button" name="next" class="btn btn-fill btn-large btn-margin-right" value="CETAK" onclick="myFunction()" />
            <a href="http://m.me/billionairestore.co.id"><input type="button" name="next" class="btn btn-accent btn-large" value="KONFIRMASI"  /></a>
        </center>
        <div class="col-md-10 col-md-offset-1">
        <form id="msform" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Reseller/beli" method="POST" >
            <!-- fieldsets -->
            <fieldset id="section-to-print">
                <div class="row" style="text-align: left;">
                  <div class="col-md-7 col-md-offset-1">
                    <a href="http://Billionairestoree.co.id"><img src="<?php echo base_url();?>assets/index/img/image/logo.png" style="height: 50px;"></a>
                  </div>
                  <div class="col-md-4">
                    <h2 style="text-align: left;">INVOICE</h2>
                    <b>Nomor Invoice : <?php echo $this->session->userdata('trsID'); ?></b>
                    <br>ID Reseller : <?php echo $this->session->userdata('resID'); ?>
                    <br>Nama Reseller : <?php echo $this->session->userdata('resNama'); ?>
                    <br>Tanggal : <?php echo $this->session->userdata('tanggal'); ?>
                   </div>
                </div>
                <div class="row">
                  <br>
                  <div class="col-md-10 col-md-offset-1">
                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Jumlah</th>
                            <th>Judul Buku</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td><?php echo $this->session->userdata('buku1'); ?></td>
                          </tr>
                          <tr>
                            <td>1</td>
                            <td><?php echo $this->session->userdata('buku2'); ?></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
                <div class="container">
                    <div class="row">
                      <div class="col-md-4 col-md-offset-1" style="text-align: left;">
                          <label>Alamat Pengiriman</label>
                          <p> <?php echo $this->session->userdata('resAlamat'); ?> </p>
                          <p> <?php echo $this->session->userdata('resDesa'); ?> </p>
                    </div>
                    <div class="row">
                      <br>
                      <div class="col-md-10 col-md-offset-1">
                          <h2>TOTAL : Rp <?php
    
                        //   $unik = $this->session->userdata('unik');
                        //   $buku1 = $this->session->userdata('harga1');
                        //   $buku2 = $this->session->userdata('harga2');
    
                        //   $total = 499000-$unik;
    
                          echo $this->session->userdata('total'); 
                          ?>
                          </h2>
                      </div>
                    </div>
                    <div class="row">
                      <br>
                      <div class="col-md-10 col-md-offset-1 rek" style="text-align: left;">
                        <label>Pembayaran :</label><br>
                        <label>BCA</label><br>
                        <label>437-234-2777</label> a.n Kiblat Pengusaha Indonesia<br>
                        KCU Ahmad Yani Bandung<br>
                        <label></label><br>
                        <label>Mandiri</label><br>
                        <label>130000-61-77771</label> a.n Kiblat Pengusaha Indonesia<br>
                        KCU Kiara Condong Bandung<br>
                      </div>
                    </div>
                </div>
              </div>
            </fieldset>
        <script>
        function myFunction() {
            window.print();
        }
        </script>
        </form>
        </div>
    </div>
</div>
<!-- /.MultiStep Form -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>

  

    <script  src="<?php echo base_url();?>assets/index/form/js/index.js"></script>
    <script  src="<?php echo base_url();?>assets/index/form/js/chain.js"></script>

<!-- bootstrap table -->
            <script src="<?php echo base_url();?>assets/index/form/js/bootstrap-table.js"></script>
            <script src="<?php echo base_url();?>assets/index/form/js/bootstrap-table-mobile.js"></script>
            <script scr="<?php echo base_url();?>assets/index/form/js/bootstrap-table-filter-control.js"></script>
            <script src="<?php echo base_url();?>assets/index/form/js/bootstrap-table-export.js"></script>
            <script type="text/javascript" src="<?php echo base_url();?>assets/index/form/js/tableExport.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                  $('form input').change(function () {
                    $('form p').text(this.files.length + " file(s) selected");
                  });
                }); 
            </script>


</body>

</html>
