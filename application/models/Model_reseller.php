<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_reseller extends CI_Model {

		public function get_all_res() {
			$query = $this->db->query('select resellers.resID, resellers.resNama,resellers.resEmail,resellers.resHp,resellers.resAlamat, provinces.name as provinsi, regencies.name AS kota_kabupaten, districts.name as kecamatan, villages.name as desa, resellers.resKodepos, transaction.trsID, transaction.tanggal, transaction.buku1, transaction.buku2, transaction.total from resellers,transaction,villages,districts,regencies,provinces WHERE resellers.resID = transaction.resID AND resellers.villageID = villages.id and villages.district_id = districts.id AND districts.regency_id = regencies.id and regencies.province_id = provinces.id');
		return $query->result();
		}
		public function get_fu_res() {
			$query = $this->db->query('select resellers.resID, resellers.resNama,resellers.resEmail,resellers.resHp, transaction.trsID, transaction.tanggal, transaction.buku1, transaction.buku2 from resellers,transaction WHERE resellers.resID = transaction.resID and transaction.tanggal < curdate()+1');
		return $query->result();
		}
		public function get_province() {
			$query = $this->db->query('select * from provinces');
		return $query->result();
		}
		public function cek_data($data) {
			$query = $this->db->get_where('resellers', $data);
			return $query;
		}
		public function cek_trs($data) {
			$query = $this->db->get_where('transaction', $data);
			return $query;
		}
		public function get_load($loadType,$loadId){
		  if($loadType=="kota"){
		   $fieldList='id,name';
		   $table='regencies';
		   $fieldName='province_id';
		   $orderByField='name';      
		  } 
		  else if ($loadType=="kecamatan") {
		   $fieldList='id,name';
		   $table='districts';
		   $fieldName='regency_id';
		   $orderByField='name';      
		  } 
		  else if ($loadType=="desa") {
		   $fieldList='id,name';
		   $table='villages';
		   $fieldName='district_id';
		   $orderByField='name';      
		  } 
		  $this->db->select($fieldList);
		  $this->db->from($table);
		  $this->db->where($fieldName, $loadId);
		  $this->db->order_by($orderByField, 'asc');
		  $query=$this->db->get();
		  return $query; 
		 }
		public function getid_reseller() {
		 $tahun = date("Ymd");
		 $today = date("Y-m-d");
		 $kode = 'RES';
		 $query = $this->db->query("SELECT MAX(resID) as max_id FROM resellers where tanggal = '$today'"); 
		 $row = $query->row_array();
		 $max_id = $row['max_id']; 
		 $max_id1 =(int) substr($max_id,11,3);
		 $res_ID = $max_id1 +1;
		 $maxres_ID = $tahun.$kode.sprintf("%03s",$res_ID);
		 return $maxres_ID;
		}
		public function get_invoice() {
		 $today = date("Y-m-d");   
		 $tahun = date("Ymd");
		 $kode = 'INV';
		 $query = $this->db->query("SELECT MAX(trsID) as max_id FROM transaction where tanggal = '$today'"); 
		 $row = $query->row_array();
		 $max_id = $row['max_id']; 
		 $max_id1 =(int) substr($max_id,11,3);
		 $trsID = $max_id1 +1;
		 $maxtrsID = $tahun.$kode.sprintf("%03s",$trsID);
		 return $maxtrsID;
		}
		public function daftar(){
			$ID = $this->getid_reseller();
			$pass = do_hash($this->input->post('password'), 'md5');
			$rt = $this->input->post('rt');
			$rw = $this->input->post('rw');
			$no = $this->input->post('noRumah');
			$detail = $this->input->post('alamat');
			
			$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			$nama = $fname.$lname;

			$alamat = $detail." RT ".$rt." RW ".$rw." Nomor ".$no;

			$data = array(
	        'resID' => $ID,
			'resNama' => $nama,
			'resEmail' => $this->input->post('email'),
			'resHp' => $this->input->post('phone'),
			'resPass' => $pass,
			'resAlamat' => $alamat,
			'villageID' => $this->input->post('desa'),
			'resKodepos' => $this->input->post('kodepos'),
			'resTanggallahir' => $this->input->post('birthdate'),
			'resProfesi' => $this->input->post('profesi'),
			'affiliate' => $this->input->post('aff'),
			'tanggal' => date('Y-m-d'),
			'resRole' => 'bronze'
			);
			$query1 = $this->db->insert('resellers', $data);

			$data = array(
				'resID' => $ID
				);
			$hasil = $this->cek_data($data);
			if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
					$sess_data['logged_in'] = 'Sudah Login';
					$sess_data['resID'] = $sess->resID;
					$sess_data['resNama'] = $sess->resNama;
					$sess_data['resEmail'] = $sess->resEmail;
					$sess_data['resHp'] = $sess->resHp;
					$sess_data['resPass'] = $sess->resPass;
					$sess_data['resAlamat'] = $sess->resAlamat;
					$sess_data['villageID'] = $sess->villageID;
					$sess_data['resKodepos'] = $sess->resKodepos;
					$sess_data['resTanggallahir'] = $sess->resTanggallahir;
					$sess_data['resProfesi'] = $sess->resProfesi;
					$sess_data['resRole'] = $sess->resRole;
					$this->session->set_userdata($sess_data);
				}
			}

			$this->beli();
		echo "<script>alert('Pendaftaran Berhasil ! Silahkan screenshot atau print invoice anda berikut ini :)');
				window.location.href='".base_url()."index.php/Reseller/pembayaran';
		 </script>";
		}
		public function beli(){
			$inv = $this->get_invoice();

			$b1 = $this->input->post('buku1');
			$b2 = $this->input->post('buku2');

			$buku1 = explode(" ", $b1);
			$buku2 = explode(" ", $b2);
//update 24b mei 2018
//kode unik menggunakan 3digit nomor invoice

		 $query = $this->db->query("SELECT MAX(trsID) as max_id FROM transaction"); 
		 $row = $query->row_array();
		 $max_id = $row['max_id']; 
		 $max_id1 =(int) substr($max_id,11,3);
		 $nomorunik = $max_id1+1;

//end of update 24 mei 2018
// 			$unik = date('s');
// 			$unik1 = (int)$unik;
// 			$nomorunik = $unik1+100;
			
			$jumlah = 499000-$nomorunik;

			$data = array(
	        'trsID' => $inv,
			'resID' => $this->session->userdata('resID'),
			'tanggal' => date('Y-m-d'),
			'buku1' => $this->input->post('buku1'),
			'buku2' => $this->input->post('buku2'),
			'bank' => $this->input->post('bank'),
			'akun' => $this->input->post('akun'),
			'rekening' => $this->input->post('norek'),
			'total' => $jumlah,
			'status' => 'pending'
			);
			$query1 = $this->db->insert('transaction', $data);

			$data = array(
				'resID' => $this->session->userdata('resID')
				);

			$hasil = $this->cek_trs($data);
			if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
					$sess_data['logged_in'] = 'Sudah Login';
					$sess_data['trsID'] = $sess->trsID;
					$sess_data['tanggal'] = $sess->tanggal;
					$sess_data['buku1'] = $sess->buku1;
					$sess_data['buku2'] = $sess->buku2;
					$sess_data['harga1'] = $sess->harga1;
					$sess_data['harga2'] = $sess->harga2;
					$sess_data['total'] = $jumlah;
					$sess_data['bank'] = $sess->bank;
					$sess_data['akun'] = $sess->akun;
					$this->session->set_userdata($sess_data);
				}
			}
			
		}
		public function tambah_bonus_tautan(){
			$ID = $this->input->post('id_eks');
			$link = $this->input->post('link_eks');

			$auth = strpos($link, "youtube");
			$auth1 = strpos($link, "youtu.be");
			$auth2 = strpos($link, "dropbox");

			if ($auth2 != 0 ) {
				$judul_raw = substr($link, 42, -5);
				$judul = str_ireplace("%20","_",$judul_raw);
			}else{
				$judul = $this->input->post('judul_eks');
			}

			if($auth == 0 && $auth1 == 0 && $auth2 == 0){
			//if ($auth == 0) {
				echo "<script>alert('Gagal mengupload link. Link yang ditambahkan harus merupakan link youtube atau dropbox. silakan masukkan kembali link anda');
				window.location.href='".base_url()."index.php/Admin/bonus';
			  </script>";	
			}else{
				$data = array(
		        'bon_ID' => $ID,
		        'bon_judul' => $judul,
		        'kat_ID' => "",
		        'bon_src' => "tautan",
		        'bon_link' => $link
				);
				$query1 = $this->db->insert('bonus', $data);
			}
		}
		// function hapus_bonus(){
		// 	$bon_ID = $this->input->post('mod_ID_delete');
		// 	$query = $this->db->query('select * from bonus where bon_ID = "'.$bon_ID.'"');

		// 	$vid_data = $query->result_array();
		// 			$data = array();
		// 				foreach ($vid_data as $key => $value) {
		// 					$data[0] = $value['bon_judul'];
		// 					$data[1] = $value['bon_src'];
		// 					//$data[0] = $value->mod_judul;
		// 					//$data[1] = $value->mod_cover;
		// 				}
		// 	if ($data[0] != "" && $data[1] == "upload") {
  //    		unlink("./assets/uploads/bonus/$data[0]");
		// 	}

		// 	$this->db->where('bon_ID', $bon_ID);
		// 	$this->db->delete('bonus');
		// }

		// function edit_bonus(){

		// 	$this->db->set('kat_ID',$this->input->post('materi_e'));
		// 	$this->db->set('bon_judul',$this->input->post('judul_e'));
		// 	$this->db->set('bon_link',$this->input->post('link_e'));
		// 	$this->db->where('bon_ID', $this->input->post('id_e'));
		// 	$this->db->update('bonus'); 
		// }
		/*
		public function get_payment() {
			$query = $this->db->query('select * from payment where status = ""');
		return $query->result();
		}
		public function turn_free() {
		$this->db->set('role','free_user');
		$this->db->update('login');  
		}
		public function turn_paid() {
		$this->db->set('role','paid_user');
		$this->db->update('login');  
		}
		public function hapus_member(){
			$this->db->where('id', $this->input->post('id'));
			$this->db->delete('login');
		}
*/
	}

?>