<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_user extends CI_Model {

		public function cek_user($data) {
			$query = $this->db->get_where('user', $data);
			return $query;
		}
		function simpan_upload($image){
			// function simpan_upload($judul,$image){
			$this->db->set('userFoto',$image);
			$this->db->where('id', $this->input->post('id_user'));
			$result=$this->db->update('user'); 
			return $result;
		}
		public function tampil_data(){
			$mbr_id = $this->session->userdata('mbr_ID');
			$query = $this->db->query("select * from user where id = '$mbr_id'");
			return $query->result();
		}
		public function user_list(){
			$query = $this->db->query('select * from user');
									// where role != "Developer"');
			return $query->result();
		}
		function edit_user(){
			$pass = $this->input->post('password');
			$password =  do_hash($pass, 'md5');

			$this->db->set('userFullname',$this->input->post('fullname'));
			$this->db->set('userEmail',$this->input->post('email'));
			$this->db->set('userTelegram',$this->input->post('telegram'));
			$this->db->set('userName',$this->input->post('username'));
			$this->db->set('userPassword',$password);
			$this->db->where('id', $this->input->post('id_user'));
			$this->db->update('user'); 
		}
		}

?>