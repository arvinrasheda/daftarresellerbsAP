function selectProvince(id_province){
		 	if(id_province!=""){
		  		loadData('kota',id_province);
		 		$("#kota_dropdown").attr('disabled', false);  
		 	}else{	
		 		$("#kota_dropdown").attr('disabled', true); 
		 	}
		}
		function selectKota(id_kota){
		 	if(id_kota!=""){
		  		loadData('kecamatan',id_kota);
		 		$("#kecamatan_dropdown").attr('disabled', false);  
		 	}else{	
		 		$("#kecamatan_dropdown").attr('disabled', true); 
		 	}
		}
		function selectKecamatan(id_kecamatan){
		 	if(id_kecamatan!=""){
		  		loadData('desa',id_kecamatan);
		 		$("#desa_dropdown").attr('disabled', false);  
		 	}else{	
		 		$("#desa_dropdown").attr('disabled', true); 
		 	}
		}
		function loadData(loadType,loadId){
		   var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
		   $("#"+loadType+"_loader").show();
		   $("#"+loadType+"_loader").fadeIn(400).html('Please wait... <img src="<?php echo base_url(); ?>assets/admin/img/loading.gif" />');
		   $.ajax({
		    type: "POST",
		    url: "index.php/Reseller/loadData",
		    data: dataString,
		    cache: false,
		    success: function(data){
		     $("#"+loadType+"_loader").hide();
		     $("#"+loadType+"_dropdown").html("<option value='-1'>-- Pilih "+loadType+" --</option>");  
		     $("#"+loadType+"_dropdown").append(data);  
		    }
		   });
		}